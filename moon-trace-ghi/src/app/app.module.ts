import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { MoonDisplayComponent } from './moon-display/moon-display.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    MoonDisplayComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
