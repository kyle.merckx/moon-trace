import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {
  title: string = 'Moon Trace'
  imageUrl: string = "https://external-preview.redd.it/t41_a4no5RbcLSUSsCyvIkZSX4wW3ruL1K-gc4T5JY4.jpg?auto=webp&s=afc4a71fc8ee76e07d2bd8e414815e34b44b9364"
  message: string = "Welcome to Moon Trace!"
}
