import { Component } from '@angular/core';

@Component({
  selector: 'app-moon-display',
  templateUrl: './moon-display.component.html',
  styleUrls: ['./moon-display.component.css']
})
export class MoonDisplayComponent {
  x_position: number = 0.0;
  y_position: number = 0.0;
  elevation: number = 0.0;
  illumination: number = 0.0;
  moon_age: number = 0.0;
  position: number = 0;
  moon_phase: string | undefined = this.getMoonPhase(this.position);
  shadow: string = `margin-left: ${this.position * 3}px`;

  setPosition(e: any) {
    if (e.target.value <= 100) {
      this.position = e.target.value;
      this.illumination = e.target.value;
    }
    else if (e.target.value > 100) {
      this.position = 100 - (e.target.value - 100);
      this.illumination = 100 - (e.target.value - 100);
    }
    this.shadow = `margin-left: ${this.position * 3}px`;
    this.moon_phase = this.getMoonPhase(this.position);
  }

  getMoonPhase(illumination: number) {
    if (illumination <= 1) {
      return "New Moon";
    }
    else if (1 < illumination && illumination < 50) {
      return "Waxing Crescent";
    }
    else if (illumination == 50) {
      return "Half Moon";
    }
    else if (50 < illumination && illumination <= 99) {
      return "Waxing Gibbous";
    }
    else if (99 < illumination && illumination <= 100 ) {
      return "Full Moon";
    }
    else if (101 < illumination && illumination < 150) {
      return "Waning Gibbous";
    }
    else if (illumination == 150) {
      return "Half Moon";
    }
    else if (150 < illumination) {
      return "Waning Crescent";
    }
    else {
      return undefined;
    }
  }
}
