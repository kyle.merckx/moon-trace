## 04/06/2023

Set up the moon-shadow to move across the screen based on the shadow value in the test component. Then implemented a user input that can control the value of the shadow.

## 04/04/2023

Started building a moon data component Learned how to use property binding and directives. Currently trying to find a suitable third-pary API that supplies moon data. Actually, I found an API that will work for now. Need to be careful, since I only get 10 requests a day.

## 04/03/2023

Initialized the ASP.NET api backend. Learned the basics of the MVC organization of the framework. I'm going to focus on the front end until I think of a good use for a backend API. I also fixed a bug in my terminal that showed up after installing Angular. The bug had something to do with an autocomplete function in zsh. Had to initialize and install the autocomplete application. Then had to delete a line pertaining to Angular in the .zshrc file. In order to find the .zshrc file I ended up adding a locate funtion to zsh that you can use to find the path to a file on your system.

## 04/01/2023

Initialized the project. Starting learning the basics of Angular components.
