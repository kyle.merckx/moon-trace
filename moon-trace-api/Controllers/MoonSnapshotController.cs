using Microsoft.AspNetCore.Mvc;
using moon_trace_api.Models;

namespace moon_trace_api.Controllers;

[ApiController]
[Route("[controller]")]
public class MoonSnapshotController : ControllerBase
{
    private readonly ILogger<MoonSnapshotController> _logger;

    public MoonSnapshotController(ILogger<MoonSnapshotController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetMoonSnapshot")]
    public IEnumerable<MoonSnapshot> Get()
    {
        return Enumerable.Range(1, 5).Select(index => new MoonSnapshot
        {
            Id = 0,
            Illumination = 0,
            MoonAge = 0,
            MoonPhase= ""
        })
        .ToArray();
    }

//     [HttpPost]
//     public async Task<ActionResult<MoonSnapshot>> PostMoonSnapshot(MoonSnapshot moonSnapshot)
//     {
//         _context.MoonSnapshots.Add(moonSnapshot);
//         await _context.SaveChangesAsync();
//     }
}
