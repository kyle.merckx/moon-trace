using Microsoft.EntityFrameworkCore;

namespace moon_trace_api.Models;

public class MoonContext : DbContext
{
    public MoonContext(DbContextOptions<MoonContext> options)
        : base(options)
        {}

    public DbSet<MoonSnapshot> MoonSnapshots { get; set; } = null!;
}
