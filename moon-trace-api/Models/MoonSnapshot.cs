namespace moon_trace_api.Models;

public class MoonSnapshot {
    public int Id { get; set; }
    public int Illumination { get; set; }
    public int MoonAge { get; set; }
    public string? MoonPhase { get; set;}

}
