## Moon Trace

Moon Trace is an app that displays moon phase and other data based on location and datetime.
This is a project I'm creating to practice working with Angular and try out some CSS experiments.

I'm planning to experiment with CSS animations to display moon phase.
Using this project to learn Angular and TypeScript.
Will build a backend (either FastAPI or I might try to learn ASP.NET for this project) that will get the moon data from an external API:
https://docs.stormglass.io/#/astronomy?id=astronomy-point-request
If the application calls for a database, I will either use PostgreSQL or MongoDB.
